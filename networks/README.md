# Example networks

These are some sample networks to test the software.

## Networks provided

### Net3.inp

Small network (97 nodes, 119 links) distributed with EPANET (as Net3.net).

### bwsn2.inp

Network with 12527 nodes and 14831 links. It corresponds to network 2 described in
[Ostfeld, A., et al. 2008. “The battle of the water sensor networks (BWSN): A design challenge for engineers and algorithms.” J. Water Resour.  Plann. Manage. 134 (6): 556–568.](https://ascelibrary.org/doi/10.1061/%28ASCE%290733-9496%282008%29134%3A6%28556%29)

Original file downloaded from http://www.water-simulation.com/wsp/wp-content/uploads/2006/02/bwsn_networks.zip (file `Example_2.inp`).

This version has been modified to skip quality simulation.

### exnet-eps50.inp

Network with 1893 nodes and 2467 links. It is based on a benchmark water system set up by the Centre for Water Systems of the University of Exeter, described in:
[Farmani, R., D. A. Savic, and G. A. Walters. 2005. "Evolutionary multi-objective optimization in water distribution network design." Eng. Optim.  37 (2): 167–183.](https://doi.org/10.1080/03052150512331303436.)     

Original file downloaded from http://emps.exeter.ac.uk/media/universityofexeter/emps/research/cws/downloads/exnet.inp

This version has been modified in order to consider a 50-day extended period simulation, with the head at reservoirs varying according to a pattern. We also eliminated node 1610, which was not connected to any other node.

## Comparing execution time with Epanet

In Linux, the execution time can be measured with the `time` command, e.g. 

```
time loopnet some-network.inp
```

In Windows, you can use e.g. `Measure-Command` from the PowerShell:

```
Measure-Command {loopnet some-network.inp}
``` 

You can do the same to measure the execution time of Epanet and compare. Results will vary depending on the machine and on Loopnet build options (especially the option to enable OpenMP parallelization).

## Our results

We obtained the execution times for networks bwsn2 and exnet-eps50 on a Linux machine with a 4-core Intel Core i5-6400 CPU. Our results showed a time reduction with respect to EPANET 2.1 between 20 and 30% with OpenMP enabled, and between 7 and 8% with OpenMP disabled.

This table shows the detailed results, with the time (in seconds) and its reduction with respect to Epanet 2.1. Network bwsn2-20 is the same as bwsn2, except that it considers a 20-day simulation (instead of one day).

Network     | epanet-2.1 | loopnet-OpenMP | loopnet-no-OpenMP 
:-----------| ---------: | -------------: | ----------------:
exnet-eps50 |       2.52 |   1.74 (31.1%) |       2.32 (8.0%)
bwsn2       |       5.95 |   4.63 (22.2%) |       5.53 (7.0%)
bwsn2-20    |      44.52 |  33.18 (25.5%) |      41.23 (7.4%)
