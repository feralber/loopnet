#
# LOOPNET -- Hydraulic & Water Quality Simulator for Water Distribution Networks
# Copyright 2018 Universitat Politecnica de Valencia
# See AUTHORS
#
# Example usage: Release build with OpenMP parallelization
#   mkdir Release
#   cd Release
#   cmake -DCMAKE_BUILD_TYPE=Release ..
#   make
#
# Example usage: Release build without OpenMP parallelization (OpenMP will 
# still be used, but only for timing).
# The same commands as above, adding -DOPENMP=OFF to the cmake command above, 
# i.e.
#   cmake -DCMAKE_BUILD_TYPE=Release -DOPENMP=OFF ..
# 
# For debug builds, set the value of CMAKE_BUILD_TYPE to Debug instead, e.g.
#   cmake -DCMAKE_BUILD_TYPE=Debug ..

cmake_minimum_required (VERSION 3.3)

function(set_openmp_flags target)
   if (OPENMP)
      target_compile_options(${target} PRIVATE "${FLAGS_COMP_OPENMP}")
   else()
      target_compile_options(${target} PRIVATE "${FLAGS_COMP_NO_OPENMP}")
   endif()
   if (OPENMP OR TIMING)   
      set_target_properties(${target} PROPERTIES LINK_FLAGS "${FLAGS_LINK_OPENMP}")
   endif()
endfunction(set_openmp_flags)

project (loopnet)

# ------------------ Compiler-specific part ------------------
#message("${CMAKE_C_COMPILER_ID}")
if (${CMAKE_C_COMPILER_ID} MATCHES GNU)
   set(FLAGS_COMP_OPENMP "-fopenmp")
   set(FLAGS_COMP_NO_OPENMP "-Wno-unknown-pragmas")
   set(FLAGS_LINK_OPENMP "-fopenmp")
   set(FLAGS_OTHER "-Wall -Wno-unused-function -Wno-unused-result -Wno-char-subscripts")
   if (MINGW)
      set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--add-stdcall-alias")
   endif()
elseif (MSVC)
   set(FLAGS_COMP_OPENMP "/openmp")
   set(FLAGS_OTHER "/W3 /wd4996")
   if ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")
      # x86 library. Use def file
      set(DEF_FILE "src/epanet2.def")
   endif()
endif()
# --------------- End of compiler-specific part -------------

# Sets for output directory for executables and libraries.
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

# ------------------ Build options ------------------
option(LOOPNET_DLL "Compile as Dynamic Link Library" ON)
option(OPENMP "Enable OpenMP parallelization" ON)
option(PRVMETH_SYM "Use symmetric matrix formulation for PRVs/PSVs" ON)
set(TIMING 1 CACHE STRING "Level of detail for time measuring:\n- 0: No time measuring\n- 1: Minimal time measuring\n- 2: Detailed time measuring")

# Less common options (not shown)
# Show all debug messages
if (NOT DEFINED DEBUG)
   set (DEBUG OFF)
endif()
# Separate links into lopped and non-looped
if (NOT DEFINED SEPARATE_LINKS)
   set (SEPARATE_LINKS ON)
endif()
# Compute flow and headloss residuals
if (NOT DEFINED RESIDUALS)
   set (RESIDUALS OFF)
endif()

# ---------------- End of build options ----------------

# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
   "${PROJECT_SOURCE_DIR}/src/config.h.in"
   "${PROJECT_BINARY_DIR}/config.h"
)
include_directories ("${PROJECT_BINARY_DIR}" "${PROJECT_SOURCE_DIR}/src")

# configure file groups
file(GLOB EPANET_SOURCES src/*.c*)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${FLAGS_OTHER}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${FLAGS_OTHER}")
set(CMAKE_C_VISIBILITY_PRESET "hidden")
set(CMAKE_CXX_VISIBILITY_PRESET "hidden")

if (LOOPNET_DLL)
   add_library(loopnetlib SHARED "${DEF_FILE}" "${EPANET_SOURCES}")
   set(LINK_LIBS "loopnetlib")
   set(MAIN_SOURCES "run/main.c")
   set_openmp_flags(loopnetlib)
   set_source_files_properties("run/main.c" PROPERTIES COMPILE_DEFINITIONS LOOPNET_DLL)
else()
   set(MAIN_SOURCES "${EPANET_SOURCES}" "run/main.c")
endif()

add_executable(loopnet ${MAIN_SOURCES})
target_link_libraries(loopnet ${LINK_LIBS})
set_openmp_flags(loopnet)
