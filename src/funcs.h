/*
**************************************************************************

 LOOPNET -- Hydraulic & Water Quality Simulator for Water Distribution Networks
 See LICENSE and AUTHORS files

 FUNCS.H -- Function Prototypes for LOOPNET

**************************************************************************
*/

#ifndef EXTERN_C
#define EXTERN_C
#endif

/*****************************************************************/
/*   Most float arguments have been changed to double - 7/3/07   */
/*****************************************************************/

/* ------- EPANET.C --------------------*/
/*
**  NOTE: The exportable functions that can be called
**        via the DLL are prototyped in TOOLKIT.H.
*/

#ifndef FUNCS_H
#define FUNCS_H

EXTERN_C void    initpointers(void);               /* Initializes pointers       */
EXTERN_C int     allocdata(void);                  /* Allocates memory           */
EXTERN_C void    freeTmplist(STmplist *);          /* Frees items in linked list */
EXTERN_C void    freeFloatlist(SFloatlist *);      /* Frees list of floats       */
EXTERN_C void    freedata(void);                   /* Frees allocated memory     */
EXTERN_C int     openfiles(char *,char *,char *);  /* Opens input & report files */
EXTERN_C int     openhydfile(void);                /* Opens hydraulics file      */
EXTERN_C int     openoutfile(void);                /* Opens binary output file   */
EXTERN_C int     strcomp(char *, char *);          /* Compares two strings       */
EXTERN_C char*   getTmpName(char* fname);          /* Gets temporary file name   */     //(2.00.12 - LR)
EXTERN_C double  interp(int, double *,             /* Interpolates a data curve  */
                          double *, double);
EXTERN_C int     findnode(char *);                 /* Finds node's index from ID */
EXTERN_C int     findlink(char *);                 /* Finds link's index from ID */
EXTERN_C char*   geterrmsg(int);                   /* Gets text of error message */
EXTERN_C void    errmsg(int);                      /* Reports program error      */
EXTERN_C void    writecon(char *);                 /* Writes text to console     */
EXTERN_C void    writewin(char *);                 /* Passes text to calling app */
EXTERN_C void    writeconf(const char *format, ...);
EXTERN_C void    initPrgOptions(void);

/* ------- INPUT1.C --------------------*/
EXTERN_C int     getdata(void);                    /* Gets network data          */
EXTERN_C void    setdefaults(void);                /* Sets default values        */
EXTERN_C void    initreport(void);                 /* Initializes report options */
EXTERN_C void    adjustdata(void);                 /* Adjusts input data         */
EXTERN_C int     inittanks(void);                  /* Initializes tank levels    */
EXTERN_C void    initunits(void);                  /* Determines reporting units */
EXTERN_C void    convertunits(void);               /* Converts data to std. units*/

/* -------- INPUT2.C -------------------*/
EXTERN_C int     netsize(void);                    /* Determines network size    */
EXTERN_C int     readdata(void);                   /* Reads in network data      */
EXTERN_C int     newline(int, char *);             /* Processes new line of data */
EXTERN_C int     addnodeID(int, char *);           /* Adds node ID to data base  */
EXTERN_C int     addlinkID(int, char *);           /* Adds link ID to data base  */
EXTERN_C int     addpattern(char *);               /* Adds pattern to data base  */
EXTERN_C int     addcurve(char *);                 /* Adds curve to data base    */
EXTERN_C STmplist *findID(char *, STmplist *);     /* Locates ID on linked list  */
EXTERN_C int     unlinked(void);                   /* Checks for unlinked nodes  */
EXTERN_C int     getpumpparams(void);              /* Computes pump curve coeffs.*/
EXTERN_C int     getpatterns(void);                /* Gets pattern data from list*/
EXTERN_C int     getcurves(void);                  /* Gets curve data from list  */
EXTERN_C int     findmatch(char *,char *[]);       /* Finds keyword in line      */
EXTERN_C int     match(char *, char *);            /* Checks for word match      */
EXTERN_C int     gettokens(char *);                /* Tokenizes input line       */
EXTERN_C int     getfloat(char *, double *);       /* Converts string to double   */
EXTERN_C double  hour(char *, char *);             /* Converts time to hours     */
EXTERN_C int     setreport(char *);                /* Processes reporting command*/
EXTERN_C void    inperrmsg(int,int,char *);        /* Input error message        */

/* ---------- INPUT3.C -----------------*/
EXTERN_C int     juncdata(void);                   /* Processes junction data    */
EXTERN_C int     tankdata(void);                   /* Processes tank data        */
EXTERN_C int     pipedata(void);                   /* Processes pipe data        */
EXTERN_C int     pumpdata(void);                   /* Processes pump data        */
EXTERN_C int     valvedata(void);                  /* Processes valve data       */
EXTERN_C int     patterndata(void);                /* Processes pattern data     */
EXTERN_C int     curvedata(void);                  /* Processes curve data       */
EXTERN_C int     coordata(void);                   /* Processes coordinate data  */
EXTERN_C int     demanddata(void);                 /* Processes demand data      */
EXTERN_C int     controldata(void);                /* Processes simple controls  */
EXTERN_C int     energydata(void);                 /* Processes energy data      */
EXTERN_C int     sourcedata(void);                 /* Processes source data      */
EXTERN_C int     emitterdata(void);                /* Processes emitter data     */
EXTERN_C int     qualdata(void);                   /* Processes quality data     */
EXTERN_C int     reactdata(void);                  /* Processes reaction data    */
EXTERN_C int     mixingdata(void);                 /* Processes tank mixing data */
EXTERN_C int     statusdata(void);                 /* Processes link status data */
EXTERN_C int     reportdata(void);                 /* Processes report options   */
EXTERN_C int     timedata(void);                   /* Processes time options     */
EXTERN_C int     optiondata(void);                 /* Processes analysis options */
EXTERN_C int     optionchoice(int);                /* Processes option choices   */
EXTERN_C int     optionvalue(int);                 /* Processes option values    */
EXTERN_C int     getpumpcurve(int);                /* Constructs a pump curve    */
EXTERN_C int     powercurve(double, double, double,/* Coeffs. of power pump curve*/
                              double, double, double *,
                              double *, double *);
EXTERN_C int     valvecheck(int, int, int);        /* Checks valve placement     */
EXTERN_C void    changestatus(int, char, double);  /* Changes status of a link   */

/* -------------- RULES.C --------------*/
EXTERN_C void    initrules(void);                  /* Initializes rule base      */
EXTERN_C void    addrule(char *);                  /* Adds rule to rule base     */
EXTERN_C int     allocrules(void);                 /* Allocates memory for rule  */
EXTERN_C int     ruledata(void);                   /* Processes rule input data  */
EXTERN_C int     checkrules(long);                 /* Checks all rules           */
EXTERN_C void    freerules(void);                  /* Frees rule base memory     */

/* ------------- REPORT.C --------------*/
EXTERN_C int     writereport(void);                /* Writes formatted report    */
EXTERN_C void    writelogo(void);                  /* Writes program logo        */
EXTERN_C void    writesummary(void);               /* Writes network summary     */
EXTERN_C void    writehydstat(int,double);          /* Writes hydraulic status    */
EXTERN_C void    writeenergy(void);                /* Writes energy usage        */
EXTERN_C int     writeresults(void);               /* Writes node/link results   */
EXTERN_C void    writeheader(int,int);             /* Writes heading on report   */
EXTERN_C void    writeline(char *);                /* Writes line to report file */
EXTERN_C void    writelinef(const char *format, ...);
EXTERN_C void    writerelerr(int, double);          /* Writes convergence error   */
EXTERN_C void    writestatchange(int,char,char);   /* Writes link status change  */
EXTERN_C void    writecontrolaction(int, int);     /* Writes control action taken*/
EXTERN_C void    writeruleaction(int, char *);     /* Writes rule action taken   */
EXTERN_C int     writehydwarn(int,double);          /* Writes hydraulic warnings  */
EXTERN_C void    writehyderr(int);                 /* Writes hydraulic error msg.*/
EXTERN_C int     disconnected(void);               /* Checks for disconnections  */
EXTERN_C void    marknodes(int, int *, char *);    /* Identifies connected nodes */
EXTERN_C void    getclosedlink(int, char *);       /* Finds a disconnecting link */
EXTERN_C void    writelimits(int,int);             /* Writes reporting limits    */
EXTERN_C int     checklimits(double *,int,int);     /* Checks variable limits     */
EXTERN_C void    writetime(char *);                /* Writes current clock time  */
EXTERN_C char    *clocktime(char *, long);         /* Converts time to hrs:min   */
EXTERN_C char    *fillstr(char *, char, int);      /* Fills string with character*/
EXTERN_C int     getnodetype(int);                 /* Determines node type       */

/* --------- HYDRAUL.C -----------------*/
EXTERN_C int     openhyd(void);                    /* Opens hydraulics solver    */

/*** Updated 3/1/01 ***/
EXTERN_C void    inithyd(int);                     /* Re-sets initial conditions */

EXTERN_C int     runhyd(long *);                   /* Solves 1-period hydraulics */
EXTERN_C int     nexthyd(long *);                  /* Moves to next time period  */
EXTERN_C void    closehyd(void);                   /* Closes hydraulics solver   */
EXTERN_C int     allocmatrix(void);                /* Allocates matrix coeffs.   */
EXTERN_C void    freematrix(void);                 /* Frees matrix coeffs.       */
EXTERN_C void    initlinkflow(int, char, double);  /* Initializes link flow      */
EXTERN_C void    setlinkflow(int, double);         /* Sets link flow via headloss*/
EXTERN_C void    setlinkstatus(int, char, char *,  /* Sets link status           */
                                 double *);
EXTERN_C void    setlinksetting(int, double,       /* Sets pump/valve setting    */
                                  char *, double *);
EXTERN_C void    resistance(int);                  /* Computes resistance coeff. */
EXTERN_C void    demands(void);                    /* Computes current demands   */
EXTERN_C int     controls(void);                   /* Controls link settings     */
EXTERN_C long    timestep(void);                   /* Computes new time step     */
EXTERN_C void    tanktimestep(long *);             /* Time till tanks fill/drain */
EXTERN_C void    controltimestep(long *);          /* Time till control action   */
EXTERN_C void    ruletimestep(long *);             /* Time till rule action      */
EXTERN_C void    addenergy(long);                  /* Accumulates energy usage   */
EXTERN_C void    getenergy(int, double *, double *); /* Computes link energy use   */
EXTERN_C void    tanklevels(long);                 /* Computes new tank levels   */
EXTERN_C double  tankvolume(int,double);           /* Finds tank vol. from grade */
EXTERN_C double  tankgrade(int,double);            /* Finds tank grade from vol. */
EXTERN_C int     netsolve(int *,double *);         /* Solves network equations   */
EXTERN_C int     badvalve(int);                    /* Checks for bad valve       */
EXTERN_C int     valvestatus(void);                /* Updates valve status       */
EXTERN_C int     linkstatus(void);                 /* Updates link status        */
EXTERN_C char    cvstatus(char,double,double);     /* Updates CV status          */
EXTERN_C char    pumpstatus(int,double);           /* Updates pump status        */
EXTERN_C char    prvstatus(int,char,double,        /* Updates PRV status         */
                             double,double);
EXTERN_C char    psvstatus(int,char,double,        /* Updates PSV status         */
                             double,double);
EXTERN_C char    fcvstatus(int,char,double,        /* Updates FCV status         */
                             double);
EXTERN_C void    tankstatus(int,int,int);          /* Checks if tank full/empty  */
EXTERN_C int     pswitch(void);                    /* Pressure switch controls   */
EXTERN_C double  newflows(const double *,          /* Updates link flows         */
                          double);
EXTERN_C void    newcoeffs(const double *);        /* Computes matrix coeffs.    */
EXTERN_C void    linkcoeffs(const double *,        /* Computes link coeffs.      */
                           int, int);
EXTERN_C void    nodecoeffs(void);                 /* Computes node coeffs.      */
EXTERN_C void    valvecoeffs(void);                /* Computes valve coeffs.     */
EXTERN_C void    pipecoeff(int, const double *);   /* Computes pipe coeff.       */
EXTERN_C double  DWcoeff(int, double *);           /* Computes D-W coeff.        */
EXTERN_C void    pumpcoeff(int, const double *);                   /* Computes pump coeff.       */

/*** Updated 10/25/00 ***/
/*** Updated 12/29/00 ***/
EXTERN_C void    curvecoeff(int,double,double *,   /* Computes curve coeffs.     */
                               double *);

EXTERN_C void    gpvcoeff(int, const double *);    /* Computes GPV coeff.        */
EXTERN_C void    pbvcoeff(int, const double *);    /* Computes PBV coeff.        */
EXTERN_C void    tcvcoeff(int, const double *);    /* Computes TCV coeff.        */
EXTERN_C void    prvcoeff(int,int,int, const double *);    /* Computes PRV coeff.        */
EXTERN_C void    psvcoeff(int,int,int, const double *);    /* Computes PSV coeff.        */
EXTERN_C void    fcvcoeff(int, const double *);    /* Computes FCV coeff.        */
//EXTERN_C void    valvecoeff(int k);
EXTERN_C void    emittercoeffs(void);              /* Computes emitter coeffs.   */
EXTERN_C double  emitflowchange(int);              /* Computes new emitter flow  */

EXTERN_C void netsolve_timeiter(int niters);

/* ----------- SMATRIX.C ---------------*/
EXTERN_C void smat_printPerf();

/* ----------- QUALITY.C ---------------*/
EXTERN_C int     openqual(void);                   /* Opens WQ solver system     */
EXTERN_C void    initqual(void);                   /* Initializes WQ solver      */
EXTERN_C int     runqual(long *);                  /* Gets current WQ results    */
EXTERN_C int     nextqual(long *);                 /* Updates WQ by hyd.timestep */
EXTERN_C int     stepqual(long *);                 /* Updates WQ by WQ time step */
EXTERN_C int     closequal(void);                  /* Closes WQ solver system    */
EXTERN_C int     gethyd(long *, long *);           /* Gets next hyd. results     */
EXTERN_C char    setReactflag(void);               /* Checks for reactive chem.  */
EXTERN_C void    transport(long);                  /* Transports mass in network */
EXTERN_C void    initsegs(void);                   /* Initializes WQ segments    */
EXTERN_C void    reorientsegs(void);               /* Re-orients WQ segments     */
EXTERN_C void    updatesegs(long);                 /* Updates quality in segments*/
EXTERN_C void    removesegs(int);                  /* Removes a WQ segment       */
EXTERN_C void    addseg(int,double,double);        /* Adds a WQ segment to pipe  */
EXTERN_C void    accumulate(long);                 /* Sums mass flow into node   */
EXTERN_C void    updatenodes(long);                /* Updates WQ at nodes        */
EXTERN_C void    sourceinput(long);                /* Computes source inputs     */
EXTERN_C void    release(long);                    /* Releases mass from nodes   */
EXTERN_C void    updatetanks(long);                /* Updates WQ in tanks        */
EXTERN_C void    updatesourcenodes(long);          /* Updates WQ at source nodes */
EXTERN_C void    tankmix1(int, long);              /* Complete mix tank model    */
EXTERN_C void    tankmix2(int, long);              /* 2-compartment tank model   */
EXTERN_C void    tankmix3(int, long);              /* FIFO tank model            */
EXTERN_C void    tankmix4(int, long);              /* LIFO tank model            */
EXTERN_C double  sourcequal(Psource);              /* Finds WQ input from source */
EXTERN_C double  avgqual(int);                     /* Finds avg. quality in pipe */
EXTERN_C void    ratecoeffs(void);                 /* Finds wall react. coeffs.  */
EXTERN_C double  piperate(int);                    /* Finds wall react. coeff.   */
EXTERN_C double  pipereact(int,double,double,long);/* Reacts water in a pipe     */
EXTERN_C double  tankreact(double,double,double,
                             long);                  /* Reacts water in a tank     */
EXTERN_C double  bulkrate(double,double,double);   /* Finds bulk reaction rate   */
EXTERN_C double  wallrate(double,double,double,double);/* Finds wall reaction rate   */

/* ------------ OUTPUT.C ---------------*/
EXTERN_C int     savenetdata(void);                /* Saves basic data to file   */
EXTERN_C int     savehyd(long *);                  /* Saves hydraulic solution   */
EXTERN_C int     savehydstep(long *);              /* Saves hydraulic timestep   */
EXTERN_C int     saveenergy(void);                 /* Saves energy usage         */
EXTERN_C int     readhyd(long *);                  /* Reads hydraulics from file */
EXTERN_C int     readhydstep(long *);              /* Reads time step from file  */
EXTERN_C int     saveoutput(void);                 /* Saves results to file      */
EXTERN_C int     nodeoutput(int, REAL4 *, double); /* Saves node results to file */
EXTERN_C int     linkoutput(int, REAL4 *, double); /* Saves link results to file */
EXTERN_C int     savefinaloutput(void);            /* Finishes saving output     */
EXTERN_C int     savetimestat(REAL4 *, char);      /* Saves time stats to file   */
EXTERN_C int     savenetreacts(double, double,
                                 double, double);    /* Saves react. rates to file */
EXTERN_C int     saveepilog(void);                 /* Saves output file epilog   */

/* ------------ INPFILE.C --------------*/
EXTERN_C int     saveinpfile(char *);              /* Saves network to text file  */

/* ------------ LOOPS.CPP --------------*/
EXTERN_C void    loops_init(void);
EXTERN_C void    loops_free(void);
EXTERN_C int     loops_linsolve(double P[], double Y[], const double H[]);
EXTERN_C double  loops_flowcorrection(int k, double q0);
EXTERN_C void    writeVector(const double v[], int n, FILE *f);
EXTERN_C void    writeVectorChar(const char v[], int n, FILE *f);
EXTERN_C void    readVector(double v[], int n, char *fname);
EXTERN_C void    loops_write_network(const char *fname);

/* ------------ RESIDUALS.C ------------*/
void compute_residuals(double *e_res, double *q_res);

#endif
